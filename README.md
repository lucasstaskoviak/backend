# DBM-Market
Projeto criado para o teste do processo seletivo DBM.

## Projetos criados:

* DBM-Market-API - Projeto API com as rotas solicitadas,
* DBM-Market-Application - Projeto de aplicação, gerencia as regras de negocio do sistema
* DBM-Market-Interface - Projeto com todas as interfaces necessarias
* DBM-Market-Domain - Coração do backend, contem as regras de negocio e objetos do sistema
* DBM-Market-Repository - Aqui é possivel encontrar todas as queries que o sistema necessita fazer
* DBM-Market-Infrastructure - Projeto responsavel por criar o banco de dados - EF Core


## Como utilizar o sistema
Antes de iniciar o sistema, é necessário criar o banco de dados. Para isso, rode o comando:

* Update-Database -StartupProject DBM-Market-Infrastructure -Project DBM-Market-Infrastructure

Caso seja necessário alterar a connection string, vá até o projeto DBM-Market-Infrastructure e abra a classe DbmMarketContextFactory e altere a variável `CONNECTIONSTRING` com a connection string desejada.

* Execute o projeto DBM-Market-API e acesse /swagger/index.html para visualizar os endpoints.

## Tecnologias utilizadas:
* .NET Core 5.0
* EF Core.
* SQL Server.

## Software utilizado:
* Microsoft Visual Studio Community 2019 - Version 16.9.1
* SQL Server Management Studio - 15.0.18369.0
