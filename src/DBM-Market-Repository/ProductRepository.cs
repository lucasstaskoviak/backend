﻿using DBM_Market_Domain;
using DBM_Market_Interface.Repository;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DBM_Market_Repository
{
    public class ProductRepository : Base,  IProductRepository
    {
        public Product GetById(int id)
        {
            return Context.Products.Include(x => x.Provider).FirstOrDefault(x => x.Id == id);
        }

        public Product GetByIdNoInclude(int id)
        {
            return Context.Products.FirstOrDefault(x => x.Id == id);
        }

        public List<Product> ListAll()
        {
            return Context.Products.Include(x => x.Provider).ToList();
        }

        public Product Create(Product product)
        {
            Context.Products.Add(product);
            Context.SaveChanges();
            return product;
        }

        public Product Update(Product product)
        {
            Context.Update(product);
            Context.SaveChanges();
            return product;
        }

        public void Delete(Product product)
        {
            Context.Products.Remove(product);
            Context.SaveChanges();
        }
    }
}
