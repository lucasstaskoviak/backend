﻿using DBM_Market_Domain;
using DBM_Market_Interface.Repository;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace DBM_Market_Repository
{
    public class ProviderRepository : Base, IProviderRepository
    {
        public Provider GetById(int id)
        {
            return Context.Providers.Include(x => x.Products).FirstOrDefault(x => x.Id == id);
        }
        public Provider GetByIdNoInclude(int id)
        {
            return Context.Providers.FirstOrDefault(x => x.Id == id);
        }

        public List<Provider> ListAll()
        {
            return Context.Providers.Include(x => x.Products).ToList();
        }

        public Provider Create(Provider provider)
        {
            Context.Providers.Add(provider);
            Context.SaveChanges();
            return provider;
        }
        public Provider Update(Provider provider)
        {
            Context.Update(provider);
            Context.SaveChanges();
            return provider;
        }

        public void Delete(Provider provider)
        {
            Context.Providers.Remove(provider);
            Context.SaveChanges();
        }
    }
}
