﻿
using DBM_Market_Infrastructure;

namespace DBM_Market_Repository
{
    public class Base
    {
        public DbmMarketContext Context;

        public Base()
        {
            var factory = new DbmMarketContextFactory();
            Context = factory.CreateDbContext(System.Array.Empty<string>());

        }
    }
}