﻿using DBM_Market_Domain;
using DBM_Market_Interface.Application;
using DBM_Market_Interface.Repository;
using System;
using System.Collections.Generic;

namespace DBM_Market_Application
{
    public class ProviderApplication : IProviderApplication
    {
        private IProviderRepository _providerRepository;


        public ProviderApplication(IProviderRepository providerRepository)
        {
            _providerRepository = providerRepository;
        }

        public Provider Create(Provider provider)
        {
            var providercreated = _providerRepository.Create(provider);
            return _providerRepository.GetById(providercreated.Id);
        }

        public void Delete(int id)
        {
            var provider = GetById(id);
            _providerRepository.Delete(provider);
        }

        public Provider GetById(int id)
        {
            return _providerRepository.GetById(id);
        }

        public List<Provider> ListAll()
        {
            return _providerRepository.ListAll();
        }

        public Provider Update(Provider provider)
        {
            var providerToUpdate = _providerRepository.GetByIdNoInclude(provider.Id);
            providerToUpdate.Name = provider.Name;
            _providerRepository.Update(providerToUpdate);
            return _providerRepository.GetById(providerToUpdate.Id);
        }
    }
}
