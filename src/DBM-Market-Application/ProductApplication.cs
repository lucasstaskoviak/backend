﻿using DBM_Market_Domain;
using DBM_Market_Interface.Application;
using DBM_Market_Interface.Repository;
using System;
using System.Collections.Generic;

namespace DBM_Market_Application
{
    public class ProductApplication : IProductApplication
    {
        private IProductRepository _productRepository;


        public ProductApplication(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public Product Create(Product product)
        {
            var productcreated = _productRepository.Create(product);
            return _productRepository.GetById(productcreated.Id);
        }

        public void Delete(int id)
        {
            var product = GetById(id);
            _productRepository.Delete(product);
        }

        public Product GetById(int id)
        {
            return _productRepository.GetById(id);
        }

        public List<Product> ListAll()
        {
            return _productRepository.ListAll();
        }

        public Product Update(Product product)
        {
            var productToUpdate = _productRepository.GetByIdNoInclude(product.Id);
            productToUpdate.Price = product.Price;
            productToUpdate.Description = product.Description;
            productToUpdate.StockQuantity = product.StockQuantity;
            productToUpdate.Provider_Id = product.Provider_Id;
            _productRepository.Update(productToUpdate);
            return _productRepository.GetById(productToUpdate.Id);
        }
    }
}
