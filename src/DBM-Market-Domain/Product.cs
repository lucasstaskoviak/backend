﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBM_Market_Domain
{
    public class Product
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public int StockQuantity { get; set; }
        public int Provider_Id { get; set; }
        public Provider Provider { get; set; }
    }
}
