﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace DBM_Market_Infrastructure
{
    public class DbmMarketContextFactory : IDesignTimeDbContextFactory<DbmMarketContext>
    {
        private const string CONNECTIONSTRING = @"data source=localhost; initial catalog=DBM-Market; persist security info = False;Integrated Security = SSPI;";

        public DbmMarketContext CreateDbContext(string[] args)
        {
            var construtor = new DbContextOptionsBuilder<DbmMarketContext>();
            construtor.UseSqlServer(CONNECTIONSTRING);

            return new DbmMarketContext(construtor.Options);
        }
    }
}
