﻿using DBM_Market_Domain;
using Microsoft.EntityFrameworkCore;

namespace DBM_Market_Infrastructure
{
    public class DbmMarketContext : DbContext
    {
        public DbmMarketContext(DbContextOptions<DbmMarketContext> option) : base(option) { }

        public DbSet<Product> Products { get; set; }
        public DbSet<Provider> Providers { get; set; }

        private static void ProductTable(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Product>(etd =>
            {
                etd.ToTable("Product");
                etd.HasKey(c => c.Id);
                etd.Property(c => c.Id).HasColumnName("Id").ValueGeneratedOnAdd();
                etd.Property(c => c.Description).HasColumnName("Description").HasMaxLength(300).IsRequired();
                etd.Property(c => c.Price).HasColumnName("Price").IsRequired();
                etd.Property(c => c.StockQuantity).HasColumnName("StockQuantity").IsRequired();
                etd.HasOne(c => c.Provider).WithMany(t => t.Products).HasForeignKey(g => g.Provider_Id);
            });
        }

        private static void ProviderTable(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Provider>(etd =>
            {
                etd.ToTable("Provider");
                etd.HasKey(c => c.Id);
                etd.Property(c => c.Id).HasColumnName("Id").ValueGeneratedOnAdd();
                etd.Property(c => c.Name).HasColumnName("Name").HasMaxLength(50).IsRequired();
            });
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.UseIdentityColumns();
            modelBuilder.HasDefaultSchema("Market");

            ProductTable(modelBuilder);
            ProviderTable(modelBuilder);
        }
    }
}
