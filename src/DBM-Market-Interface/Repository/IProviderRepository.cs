﻿using DBM_Market_Domain;
using System.Collections.Generic;

namespace DBM_Market_Interface.Repository
{
    public interface IProviderRepository
    {
        List<Provider> ListAll();

        Provider GetById(int id);

        Provider Create(Provider provider);

        Provider Update(Provider provider);

        Provider GetByIdNoInclude(int id);

        void Delete(Provider provider);
    }
}
