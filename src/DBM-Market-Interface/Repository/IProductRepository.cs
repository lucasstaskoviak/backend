﻿using DBM_Market_Domain;
using System.Collections.Generic;

namespace DBM_Market_Interface.Repository
{
    public interface IProductRepository
    {
        List<Product> ListAll();

        Product GetById(int id);

        Product Create(Product product);

        Product Update(Product product);

        Product GetByIdNoInclude(int id);

        void Delete(Product product);
    }
}
