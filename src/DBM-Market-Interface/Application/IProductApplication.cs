﻿using DBM_Market_Domain;
using System.Collections.Generic;

namespace DBM_Market_Interface.Application
{
    public interface IProductApplication
    {
        List<Product> ListAll();

        Product GetById(int id);

        Product Create(Product product);

        Product Update(Product product);
        void Delete(int id);
    }
}
