﻿using DBM_Market_Domain;
using System.Collections.Generic;

namespace DBM_Market_Interface.Application
{
    public interface IProviderApplication
    {
        List<Provider> ListAll();

        Provider GetById(int id);

        Provider Create(Provider provider);

        Provider Update(Provider provider);
        void Delete(int id);
    }
}
