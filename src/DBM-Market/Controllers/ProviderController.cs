﻿using DBM_Market.ViewModels;
using DBM_Market_Interface.Application;
using Microsoft.AspNetCore.Mvc;

namespace DBM_Market
{
    [ApiController]
    [Route("provider")]
    public class ProviderController : ControllerBase
    {
        private IProviderApplication _providerApplication;

        public ProviderController(IProviderApplication providerApplication)
        {
            _providerApplication = providerApplication;
        }


        [HttpGet]
        [Route("All")]
        public IActionResult ListAll()
        {
            var providers = _providerApplication.ListAll();
            var providertViewModel = ProviderViewModelExtension.ConvertToViewModels(providers);
            return Ok(providertViewModel);
        }

        [HttpGet]
        public IActionResult GetById(int id)
        {
            var provider = _providerApplication.GetById(id);

            return Ok(provider.ConvertToViewModel());
        }

        [HttpPost]
        public IActionResult Create(ProviderCreateViewModel providerViewModel)
        {
            var provider = providerViewModel.ConvertToObject();
            var providercreated = _providerApplication.Create(provider);

            return Created("Created", providercreated.ConvertToViewModel());
        }

        [HttpPut]
        public IActionResult Update(ProviderUpdateViewModel providerViewModel)
        {
            var provider = providerViewModel.ConvertToObject();
            var providerupdated = _providerApplication.Update(provider);

            return Ok(providerupdated.ConvertToViewModel());
        }

        [HttpDelete]
        public IActionResult Delete(int id)
        {
            _providerApplication.Delete(id);
            return NoContent();
        }


    }
}