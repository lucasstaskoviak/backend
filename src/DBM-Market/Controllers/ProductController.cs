﻿using DBM_Market.ViewModels;
using DBM_Market_Interface.Application;
using Microsoft.AspNetCore.Mvc;

namespace DBM_Market
{
    [ApiController]
    [Route("product")]
    public class ProductController : ControllerBase
    {
        private IProductApplication _productApplication;

        public ProductController(IProductApplication productApplication)
        {
            _productApplication = productApplication;
        }


        [HttpGet]
        [Route("All")]
        public IActionResult ListAll()
        {
            var products = _productApplication.ListAll();
            var productViewModel = ProductViewModelExtension.ConvertToViewModels(products);
            return Ok(productViewModel);
        }

        [HttpGet]
        public IActionResult GetById(int id)
        {
            var product = _productApplication.GetById(id);

            return Ok(product.ConvertToViewModel());
        }

        [HttpPost]
        public IActionResult Create(ProductCreateViewModel productViewModel)
        {
            var product = productViewModel.ConvertToObject();
            var productcreated = _productApplication.Create(product);

            return Created("Created", productcreated.ConvertToViewModel());
        }

        [HttpPut]
        public IActionResult Update(ProductUpdateViewModel productViewModel)
        {
            var product = productViewModel.ConvertToObject();
            var productupdated = _productApplication.Update(product);

            return Ok(productupdated.ConvertToViewModel());
        }

        [HttpDelete]
        public IActionResult Delete(int id)
        {
            _productApplication.Delete(id);
            return NoContent();
        }
    }
}