﻿using DBM_Market_Domain;
using System.Collections.Generic;
using System.Linq;

namespace DBM_Market.ViewModels
{
    public class ProviderViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public List<ProductViewModel> Products { get; set; }
    }
    public static class ProviderViewModelExtension
    {
        public static ProviderViewModel ConvertToViewModel(this Provider provider)
        {
            var providerViewModel = new ProviderViewModel
            {
                Id = provider.Id,
                Name = provider.Name
            };
            if (provider.Products != null)
                providerViewModel.Products = ProductViewModelExtension.ConvertToViewModelsNoInclude(provider.Products);
            return providerViewModel;
        }
        public static List<ProviderViewModel> ConvertToViewModels(List<Provider> providers)
        {
            var result = new List<ProviderViewModel>();

            foreach (var provider in providers)
            {
                result.Add(provider.ConvertToViewModel());
            }

            return result;
        }

        public static Provider ConvertToObject(this ProviderViewModel provider)
        {
            return new Provider
            {
                Id = provider.Id,
                Name = provider.Name
            };
        }

        public static List<Provider> ConvertToObject(List<ProviderViewModel> providerViewModels)
        {
            var result = new List<Provider>();

            foreach (var provider in providerViewModels)
            {
                result.Add(provider.ConvertToObject());
            }

            return result;
        }
    }
}
