﻿using DBM_Market_Domain;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace DBM_Market.ViewModels
{
    public class ProviderCreateViewModel
    {
        [Required]
        [MinLength(10)]
        [MaxLength(50)]
        public string Name { get; set; }
    }
    public static class ProviderCreateViewModelExtension
    {

        public static Provider ConvertToObject(this ProviderCreateViewModel provider)
        {
            return new Provider
            {
                Name = provider.Name
            };
        }

        public static List<Provider> ConvertToObject(List<ProviderCreateViewModel> providerViewModels)
        {
            var result = new List<Provider>();

            foreach (var provider in providerViewModels)
            {
                result.Add(provider.ConvertToObject());
            }

            return result;
        }
    }
}
