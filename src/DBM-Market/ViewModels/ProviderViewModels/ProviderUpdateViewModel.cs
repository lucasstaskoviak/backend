﻿using DBM_Market_Domain;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace DBM_Market.ViewModels
{
    public class ProviderUpdateViewModel
    {
        [Required]
        public int Id { get; set; }
        [Required]
        [MinLength(10)]
        [MaxLength(50)]
        public string Name { get; set; }
    }
    public static class ProviderUpdateViewModelExtension
    {

        public static Provider ConvertToObject(this ProviderUpdateViewModel provider)
        {
            return new Provider
            {
                Id = provider.Id,
                Name = provider.Name
            };
        }

        public static List<Provider> ConvertToObject(List<ProviderUpdateViewModel> providerViewModels)
        {
            var result = new List<Provider>();

            foreach (var provider in providerViewModels)
            {
                result.Add(provider.ConvertToObject());
            }

            return result;
        }
    }
}
