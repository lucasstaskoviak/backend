﻿using DBM_Market_Domain;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace DBM_Market.ViewModels
{
    public class ProductCreateViewModel
    {
        [Required]
        [MinLength(10)]
        [MaxLength(300)]
        public string Description { get; set; }
        [Required]
        public decimal Price { get; set; }
        [Required]
        [Range(1, int.MaxValue)]
        public int StockQuantity { get; set; }
        [Required]
        public int Provider_Id { get; set; }
    }
    public static class ProductCreateViewModelExtension
    {

        public static Product ConvertToObject(this ProductCreateViewModel product)
        {
            return new Product
            {
                Description = product.Description,
                Price = product.Price,
                StockQuantity = product.StockQuantity,
                Provider_Id = product.Provider_Id
            };
        }

        public static List<Product> ConvertToObject(List<ProductCreateViewModel> productViewModels)
        {
            var result = new List<Product>();

            foreach (var product in productViewModels)
            {
                result.Add(product.ConvertToObject());
            }

            return result;
        }
    }
}
