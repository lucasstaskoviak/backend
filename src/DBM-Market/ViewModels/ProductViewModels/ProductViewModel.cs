﻿using DBM_Market_Domain;
using System.Collections.Generic;
using System.Linq; 

namespace DBM_Market.ViewModels
{
    public class ProductViewModel
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public int StockQuantity { get; set; }
        public int Provider_Id { get; set; }
        public ProviderViewModel Provider { get; set; }

    }
    public static class ProductViewModelExtension
    {
        public static ProductViewModel ConvertToViewModel(this Product product)
        {
            var productViewModel = new ProductViewModel
            {
                Id = product.Id,
                Description = product.Description,
                Price = product.Price,
                StockQuantity = product.StockQuantity,
                Provider_Id = product.Provider_Id
            };
            if (product.Provider != null)
                productViewModel.Provider = product.Provider.ConvertToViewModel();
            return productViewModel;
        }
        public static List<ProductViewModel> ConvertToViewModels(List<Product> products)
        {
            var result = new List<ProductViewModel>();

            foreach (var product in products)
            {
                result.Add(product.ConvertToViewModel());
            }

            return result;
        }

        public static Product ConvertToObject(this ProductViewModel product)
        {
            return new Product
            {
                Id = product.Id,
                Description = product.Description,
                Price = product.Price,
                StockQuantity = product.StockQuantity,
                Provider_Id = product.Provider_Id
            };
        }

        public static List<Product> ConvertToObject(List<ProductViewModel> productViewModels)
        {
            var result = new List<Product>();

            foreach (var product in productViewModels)
            {
                result.Add(product.ConvertToObject());
            }

            return result;
        }
        public static ProductViewModel ConvertToViewModelNoInclude(this Product product)
        {
            var productViewModel = new ProductViewModel
            {
                Id = product.Id,
                Description = product.Description,
                Price = product.Price,
                StockQuantity = product.StockQuantity,
                Provider_Id = product.Provider_Id
            };
            return productViewModel;
        }
        public static List<ProductViewModel> ConvertToViewModelsNoInclude(List<Product> products)
        {
            var result = new List<ProductViewModel>();

            foreach (var product in products)
            {
                result.Add(product.ConvertToViewModelNoInclude());
            }

            return result;
        }

    }
}
